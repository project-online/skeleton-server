# dotnet-install
if (Test-Path "artifacts\") {
	Remove-Item "artifacts\" -Recurse -Force
}

dotnet restore src/
dotnet build src/ --output artifacts/ --no-restore /p:BuildNumber=999

#Copy the data in the data dir to the artificats location
Copy-Item -Path ".\data\*" -Destination "artifacts\" 
Remove-Item "artifacts\CitizenFX.Core.*" -Recurse -Force
